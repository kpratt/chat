package main

import (
	"flag"
	"fmt"
	"net/http"
	"bitbucket.org/kpratt/chat/module"
	"bitbucket.org/kpratt/nom-de-comm/types"
	"github.com/google/uuid"
	"bitbucket.org/kpratt/nom-de-comm/log"
	"go.uber.org/zap"
	"encoding/hex"
)

var port = flag.Uint("port", 8080, "The port to open the server on.")

func main() {
	flag.Parse()

	f2f := &mockF2F{
		packets: make(chan *types.Frame),
	}

	fmt.Printf("Listening on port %d.\n", *port)
	go func() {
		defer log.Sync()
		log.Info(
			"http server",
			zap.Error(http.ListenAndServe(fmt.Sprintf(":%d", *port), chat_module.AccessLogger(chat_module.Router(f2f)))),
		)
	}()

	for f2f.f == nil {
	}

	var fp types.FP
	_, err := hex.Decode(fp[:], []byte("AAAAAAAABBBBBBBBCCCCCCCCDDDDDDDDAAAAAAAABBBBBBBBCCCCCCCCDDDDDDDD"))
	if err != nil {
		panic(err)
	}
	fmt.Println("[Enter] to send friend arrival")
	fmt.Scanln()
	f2f.f(fp, true)

	fmt.Println("[Enter] to send msg")
	fmt.Scanln()
	f2f.packets <- &types.Frame{Payload: []byte("Hello Friend o mine!")}

	fmt.Println("Server exiting.")
	fmt.Scanln()

}

type mockF2F struct {
	f       func(fp types.FP, online bool)
	packets chan *types.Frame
}

func (f2f *mockF2F) WhoAmI() (fp types.FP) {
	return
}

func (f2f *mockF2F) Listen(f func(fp types.FP, online bool)) {
	f2f.f = f
}

func (f2f *mockF2F) Send(dst types.FP, dstApp uuid.UUID, payload []byte) error {
	defer log.Sync()
	log.Info("f2f.Send", zap.String("text", string(payload)))
	return nil
}

func (f2f *mockF2F) Recv() (*types.Frame, error) {
	return <-f2f.packets, nil
}

func (f2f *mockF2F) Close() error {
	return nil
}
