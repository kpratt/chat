# LEGEND
# ==========
# #  line comment
# -  ignore return code
# $$ escape $
# @  don't print command

HOMES := ./tmp

.PHONY: all test clean debug mock-server
test:
	@go test -timeout 3s ./module

bindata:
	go-bindata -pkg assets -prefix assets -o module/assets/assets.go ./assets

server:
	go run ./cmd/chat/chat.go -port 9090

install: bindata
	go install ./cmd/chat/

mock-server:
	go run mock-server/mock-server.go -port 9090

ci:
	@go test -timeout 30s ./module

grok:
	@ngrok http 9090 --region au

proto:
	@mkdir -p ./module/protocol
	@protoc --go_out=./module/protocol ./proto/*.proto

boot2: _daemonA _daemonB _informA
	@mkdir -p ./tmp

_daemonA:
	@echo ""
	@echo "Spawning Daemon A"
	@f2f-linkd -appPort 6007 -peerPort 3007 -home $(HOMES)/A > $(HOMES)/A.link.log &
	@sleep 2
	@go run Main.go -port 9091 -link-port 6007 > $(HOMES)/A.server.log &

_daemonB:
	@echo ""
	@echo "Spawning Daemon B"
	@f2f-linkd -appPort 6008 -peerPort 3008 -home $(HOMES)/B > $(HOMES)/B.log &
	@sleep 2
	@go run Main.go -port 9092 -link-port 6008 > $(HOMES)/B.server.log &

_informA:
	@f2f-attempt -fp f510267207641ecd11d53419f5fe2c134b31ea0c9275b6eb793aa9d12acd00f9 -ip 127.0.0.1 -port 3008 -appPort 6007

register: _registerA _registerB

_registerA:
	@echo ""
	@echo "Exporting A"
	@f2f-export -f A -home $(HOMES)/A
	@echo ""
	@echo "Importing A"
	@f2f-import -f A -home $(HOMES)/B

_registerB:
	@echo ""
	@echo "Exporting B"
	@f2f-export -f B -home $(HOMES)/B
	@echo ""
	@echo "Importing B"
	@f2f-import -f B -home $(HOMES)/A

kill-boot2:
	-ps aux | grep f2f | awk '{ print $$2 }' | xargs kill >/dev/null 2>&1
	-ps aux | grep '\-port 9091' | awk '{ print $$2 }' | xargs kill >/dev/null
	-ps aux | grep '\-port 9092' | awk '{ print $$2 }' | xargs kill >/dev/null

