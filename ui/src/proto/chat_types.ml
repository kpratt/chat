[@@@ocaml.warning "-27-30-39"]


type identity = {
  fp : string;
}

type msg = {
  id : int32;
  friend : string;
  sender : string;
  text : string;
}

type friend_event_etype =
  | Arrival 
  | Departure 

type friend_event = {
  event_type : friend_event_etype;
  friend : string;
}

type frame =
  | Id of identity
  | Msg of msg
  | Friend_event of friend_event

let rec default_identity 
  ?fp:((fp:string) = "")
  () : identity  = {
  fp;
}

let rec default_msg 
  ?id:((id:int32) = 0l)
  ?friend:((friend:string) = "")
  ?sender:((sender:string) = "")
  ?text:((text:string) = "")
  () : msg  = {
  id;
  friend;
  sender;
  text;
}

let rec default_friend_event_etype () = (Arrival:friend_event_etype)

let rec default_friend_event 
  ?event_type:((event_type:friend_event_etype) = default_friend_event_etype ())
  ?friend:((friend:string) = "")
  () : friend_event  = {
  event_type;
  friend;
}

let rec default_frame () : frame = Id (default_identity ())
