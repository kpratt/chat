(** chat.proto Types *)



(** {2 Types} *)

type identity = {
  fp : string;
}

type msg = {
  id : int32;
  friend : string;
  sender : string;
  text : string;
}

type friend_event_etype =
  | Arrival 
  | Departure 

type friend_event = {
  event_type : friend_event_etype;
  friend : string;
}

type frame =
  | Id of identity
  | Msg of msg
  | Friend_event of friend_event


(** {2 Default values} *)

val default_identity : 
  ?fp:string ->
  unit ->
  identity
(** [default_identity ()] is the default value for type [identity] *)

val default_msg : 
  ?id:int32 ->
  ?friend:string ->
  ?sender:string ->
  ?text:string ->
  unit ->
  msg
(** [default_msg ()] is the default value for type [msg] *)

val default_friend_event_etype : unit -> friend_event_etype
(** [default_friend_event_etype ()] is the default value for type [friend_event_etype] *)

val default_friend_event : 
  ?event_type:friend_event_etype ->
  ?friend:string ->
  unit ->
  friend_event
(** [default_friend_event ()] is the default value for type [friend_event] *)

val default_frame : unit -> frame
(** [default_frame ()] is the default value for type [frame] *)
