[@@@ocaml.warning "-27-30-39"]

type identity_mutable = {
  mutable fp : string;
}

let default_identity_mutable () : identity_mutable = {
  fp = "";
}

type msg_mutable = {
  mutable id : int32;
  mutable friend : string;
  mutable sender : string;
  mutable text : string;
}

let default_msg_mutable () : msg_mutable = {
  id = 0l;
  friend = "";
  sender = "";
  text = "";
}

type friend_event_mutable = {
  mutable event_type : Chat_types.friend_event_etype;
  mutable friend : string;
}

let default_friend_event_mutable () : friend_event_mutable = {
  event_type = Chat_types.default_friend_event_etype ();
  friend = "";
}


let rec decode_identity json =
  let v = default_identity_mutable () in
  let keys = Js_dict.keys json in
  let last_key_index = Array.length keys - 1 in
  for i = 0 to last_key_index do
    match Array.unsafe_get keys i with
    | "fp" -> 
      let json = Js_dict.unsafeGet json "fp" in
      v.fp <- Pbrt_bs.string json "identity" "fp"
    
    | _ -> () (*Unknown fields are ignored*)
  done;
  ({
    Chat_types.fp = v.fp;
  } : Chat_types.identity)

let rec decode_msg json =
  let v = default_msg_mutable () in
  let keys = Js_dict.keys json in
  let last_key_index = Array.length keys - 1 in
  for i = 0 to last_key_index do
    match Array.unsafe_get keys i with
    | "id" -> 
      let json = Js_dict.unsafeGet json "id" in
      v.id <- Pbrt_bs.int32 json "msg" "id"
    | "friend" -> 
      let json = Js_dict.unsafeGet json "friend" in
      v.friend <- Pbrt_bs.string json "msg" "friend"
    | "sender" -> 
      let json = Js_dict.unsafeGet json "sender" in
      v.sender <- Pbrt_bs.string json "msg" "sender"
    | "text" -> 
      let json = Js_dict.unsafeGet json "text" in
      v.text <- Pbrt_bs.string json "msg" "text"
    
    | _ -> () (*Unknown fields are ignored*)
  done;
  ({
    Chat_types.id = v.id;
    Chat_types.friend = v.friend;
    Chat_types.sender = v.sender;
    Chat_types.text = v.text;
  } : Chat_types.msg)

let rec decode_friend_event_etype (json:Js_json.t) =
  match Pbrt_bs.string json "friend_event_etype" "value" with
  | "Arrival" -> Chat_types.Arrival
  | "Departure" -> Chat_types.Departure
  | "" -> Chat_types.Arrival
  | _ -> Pbrt_bs.E.malformed_variant "friend_event_etype"

let rec decode_friend_event json =
  let v = default_friend_event_mutable () in
  let keys = Js_dict.keys json in
  let last_key_index = Array.length keys - 1 in
  for i = 0 to last_key_index do
    match Array.unsafe_get keys i with
    | "eventType" -> 
      let json = Js_dict.unsafeGet json "eventType" in
      v.event_type <- (decode_friend_event_etype json)
    | "friend" -> 
      let json = Js_dict.unsafeGet json "friend" in
      v.friend <- Pbrt_bs.string json "friend_event" "friend"
    
    | _ -> () (*Unknown fields are ignored*)
  done;
  ({
    Chat_types.event_type = v.event_type;
    Chat_types.friend = v.friend;
  } : Chat_types.friend_event)

let rec decode_frame json =
  let keys = Js_dict.keys json in
  let rec loop = function 
    | -1 -> Pbrt_bs.E.malformed_variant "frame"
    | i -> 
      begin match Array.unsafe_get keys i with
      | "id" -> 
        let json = Js_dict.unsafeGet json "id" in
        Chat_types.Id ((decode_identity (Pbrt_bs.object_ json "frame" "Id")))
      | "msg" -> 
        let json = Js_dict.unsafeGet json "msg" in
        Chat_types.Msg ((decode_msg (Pbrt_bs.object_ json "frame" "Msg")))
      | "friendEvent" -> 
        let json = Js_dict.unsafeGet json "friendEvent" in
        Chat_types.Friend_event ((decode_friend_event (Pbrt_bs.object_ json "frame" "Friend_event")))
      
      | _ -> loop (i - 1)
      end
  in
  loop (Array.length keys - 1)

let rec encode_identity (v:Chat_types.identity) = 
  let json = Js_dict.empty () in
  Js_dict.set json "fp" (Js_json.string v.Chat_types.fp);
  json

let rec encode_msg (v:Chat_types.msg) = 
  let json = Js_dict.empty () in
  Js_dict.set json "id" (Js_json.number (Int32.to_float v.Chat_types.id));
  Js_dict.set json "friend" (Js_json.string v.Chat_types.friend);
  Js_dict.set json "sender" (Js_json.string v.Chat_types.sender);
  Js_dict.set json "text" (Js_json.string v.Chat_types.text);
  json

let rec encode_friend_event_etype (v:Chat_types.friend_event_etype) : string = 
  match v with
  | Chat_types.Arrival -> "Arrival"
  | Chat_types.Departure -> "Departure"

let rec encode_friend_event (v:Chat_types.friend_event) = 
  let json = Js_dict.empty () in
  Js_dict.set json "eventType" (Js_json.string (encode_friend_event_etype v.Chat_types.event_type));
  Js_dict.set json "friend" (Js_json.string v.Chat_types.friend);
  json

let rec encode_frame (v:Chat_types.frame) = 
  let json = Js_dict.empty () in
  begin match v with
  | Chat_types.Id v ->
    begin (* id field *)
      let json' = encode_identity v in
      Js_dict.set json "id" (Js_json.object_ json');
    end;
  | Chat_types.Msg v ->
    begin (* msg field *)
      let json' = encode_msg v in
      Js_dict.set json "msg" (Js_json.object_ json');
    end;
  | Chat_types.Friend_event v ->
    begin (* friendEvent field *)
      let json' = encode_friend_event v in
      Js_dict.set json "friendEvent" (Js_json.object_ json');
    end;
  end;
  json
