(** chat.proto BuckleScript Encoding *)


(** {2 Protobuf JSON Encoding} *)

val encode_identity : Chat_types.identity -> Js_json.t Js_dict.t
(** [encode_identity v dict] encodes [v] int the given JSON [dict] *)

val encode_msg : Chat_types.msg -> Js_json.t Js_dict.t
(** [encode_msg v dict] encodes [v] int the given JSON [dict] *)

val encode_friend_event_etype : Chat_types.friend_event_etype -> string
(** [encode_friend_event_etype v] returns JSON string*)

val encode_friend_event : Chat_types.friend_event -> Js_json.t Js_dict.t
(** [encode_friend_event v dict] encodes [v] int the given JSON [dict] *)

val encode_frame : Chat_types.frame -> Js_json.t Js_dict.t
(** [encode_frame v dict] encodes [v] int the given JSON [dict] *)


(** {2 BS Decoding} *)

val decode_identity : Js_json.t Js_dict.t -> Chat_types.identity
(** [decode_identity decoder] decodes a [identity] value from [decoder] *)

val decode_msg : Js_json.t Js_dict.t -> Chat_types.msg
(** [decode_msg decoder] decodes a [msg] value from [decoder] *)

val decode_friend_event_etype : Js_json.t -> Chat_types.friend_event_etype
(** [decode_friend_event_etype value] decodes a [friend_event_etype] from a Json value*)

val decode_friend_event : Js_json.t Js_dict.t -> Chat_types.friend_event
(** [decode_friend_event decoder] decodes a [friend_event] value from [decoder] *)

val decode_frame : Js_json.t Js_dict.t -> Chat_types.frame
(** [decode_frame decoder] decodes a [frame] value from [decoder] *)
