
let of_ = (msg: string): Chat_types.msg => {
	msg |> Js.Json.parseExn
      |> Js.Json.decodeObject
      |> Unsafe.unwrap
      |> Chat_bs.decode_msg
};

let ofFrame = (msg: string): Chat_types.frame => {
  msg
      |> Js.Json.parseExn
      |> Js.Json.decodeObject
      |> Unsafe.unwrap
      |> Chat_bs.decode_frame
};
