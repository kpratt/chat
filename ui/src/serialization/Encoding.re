
let of_ = (msg: Chat_types.msg): string => {
	msg |> Chat_bs.encode_msg
      |> (x) => Js.Json.object_(x)
      |> Js.Json.stringify
};


let ofFrame = (msg: Chat_types.frame): string => {
	msg |> Chat_bs.encode_frame
      |> (x) => Js.Json.object_(x)
      |> Js.Json.stringify
};
