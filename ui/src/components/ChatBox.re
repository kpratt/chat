let component = ReasonReact.statelessComponent("ChatBox");

let styles = ReactDOMRe.Style.make(
	~margin="30px auto",
	~width="80%",
	~height="60%",
	())


let onEnter = (eventHandler: Types.actions => unit) => (str: string): unit => {
	eventHandler(Types.MessageSent(str))
};

let make = (~eventHandler, _children) => {
    ...component,

	render: _ => {
			<div id={"chatbox"} style={styles}>
				<TextBox onEnter={onEnter(eventHandler)} />
			</div>;
    },
};
