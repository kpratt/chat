open Types;

let component = ReasonReact.reducerComponent("Application");

let body = ReactDOMRe.Style.make(
	~display="block",
	~height="100%",
	~width="100%",
	~padding="24px 0",
  ~backgroundColor="#224",
	~color="#BBB",
	());

let applicationStyle = ReactDOMRe.Style.make(
	~display="flex",
	~margin="auto",
	~flexDirection="row",
  ~height="700px",
  ~width="1260px",
  ~border="1px solid black",
	());


let friendListStyle = ReactDOMRe.Style.make(
	~flex="0 0 300px",
	~border="2px solid black",
	~padding="8px",
	~overflow="hidden",
	());

let contentBodyStyle = ReactDOMRe.Style.make(
	~display="flex",
	~flex="1 1 auto",
	~flexDirection="column",
	~width="900px",
	())

let messageHistoryStyle = ReactDOMRe.Style.make(
	~flex="1 1 auto",
	~border="2px solid black",
	~height="500px",
	());

let chatBoxStyle = ReactDOMRe.Style.make(
	~flex="0 0 auto",
	~border="2px solid black",
	~height="160px",
	());


let compareMsgs = (a: Chat_types.msg, b: Chat_types.msg) => Int32.compare(a.id, b.id);

let notEqualMsgs = (a: Chat_types.msg, b: Chat_types.msg) => Int32.compare(a.id, b.id) != 0;

let rec filterUniq = (msgs: list(Chat_types.msg)): list(Chat_types.msg) => {
  let newMsgs =
    switch (msgs) {
    | [] => []
    | [a, ...rest] => List.append([a], List.filter(notEqualMsgs(a), filterUniq(rest)))
    };
  newMsgs;
};

let appendMessage = (rooms, user, msg) =>
  switch (safeFind(user, rooms)) {
  | None => rooms
  | Some(room) =>
    let msgs = List.append(room.messages, [msg]) |> List.sort(compareMsgs) |> filterUniq;
    let newRooms = StringMap.add(user, {...room, messages: msgs}, rooms);
    newRooms;
  };

let currentUser = (state: Types.state): string =>
  switch (state.currentWindow) {
  | None => "..."
  | Some(user) => user
  };

let currentMessages = (state: Types.state): list(Chat_types.msg) => {
  let msgs: list(Chat_types.msg) =
    switch (state.currentWindow) {
    | None => []
    | Some(user) =>
      switch (safeFind(user, state.rooms)) {
      | None => []
      | Some(room) => room.messages
      }
    };
  msgs;
};

let sendRoomChanged = (send, friend) => send(RoomChange(friend));

type state = Types.state;

type actions = Types.actions;

let make = _children => {
  ...component,
  didMount: self => {
    Js.log("Application did mount.");
    self.send(CreateSocket);
  },
  initialState: () => {
    fp: "",
    currentWindow: Some("self"),
    rooms: StringMap.singleton("self", {friend: "self", messages: []}),
    friends: ["self"],
    post: None,
  },
  reducer: (action: actions, state: state) =>
    switch (action) {
    | CreateSocket =>
      ReasonReact.SideEffects(
        (
          self => {
            let post = WS.makeChatSocket(self.send);
            self.send(NewSocket(post));
          }
        ),
      )
    | NewSocket(post) => ReasonReact.Update({...state, post: Some(post)})
    | WhoAmI(fp) => ReasonReact.Update({...state, fp})
    | RoomChange(friend) => ReasonReact.Update({...state, currentWindow: Some(friend)})
    | MessageRecieved(msg) => ReasonReact.Update({...state, rooms: appendMessage(state.rooms, msg.sender, msg)})
    | MessageSent(text) =>
      switch (state.currentWindow) {
      | None => ReasonReact.Update(state)
      | Some(user) =>
        let t = Js.Date.make() |> Js.Date.getTime;
        let msg: Chat_types.msg = {
          Chat_types.id: Int32.of_float(t),
          Chat_types.friend: user,
          Chat_types.sender: state.fp,
          Chat_types.text,
        };
        msg |> Option.default(Option.noop, state.post) |> ignore;
        let msgs = appendMessage(state.rooms, msg.friend, msg);
        ReasonReact.Update({...state, rooms: msgs});
      }
    | FriendArrived(user) =>
      let newFriendList = List.sort(compare, List.append([user], state.friends));
      let newMessages =
        switch (safeFind(user, state.rooms)) {
        | None => StringMap.add(user, {friend: user, messages: []}, state.rooms)
        | Some(_) => state.rooms
        };
      ReasonReact.Update({...state, friends: newFriendList, rooms: newMessages});
    | FriendLeft(user) =>
      let newFriendList = List.filter(x => x != user, state.friends);
      ReasonReact.Update({...state, friends: newFriendList});
    | NoOp => ReasonReact.Update(state)
    },
  render: self =>
		<div style={body}>
			<div style={applicationStyle}>
				<div style={friendListStyle}> <FriendList onFriendSelect={sendRoomChanged(self.send)} friends={self.state.friends} /> </div>
				<div style={contentBodyStyle}>
					<div style={messageHistoryStyle}> <MessageHistory user={currentUser(self.state)} messages={currentMessages(self.state)} /> </div>
					<div style={chatBoxStyle}> <ChatBox eventHandler={self.send} /> </div>
				</div>
			</div>
    </div>,
};
