
let headerStyle = ReactDOMRe.Style.make(
	~width="960px",
	~maxWidth="90%",
	~overflow="hidden",
	~padding="16px",
	());

let liStyle = (ab: bool) => if(ab){
		ReactDOMRe.Style.make(
			~backgroundColor="#448",
			());
	} else {
		ReactDOMRe.Style.make(
			~backgroundColor="#336",
			());
	}

let entryStyle = ReactDOMRe.Style.make(
	~display="flex",
	~padding="8px",
	~width="960px",
	~maxWidth="100%",
	());

let ulStyle = ReactDOMRe.Style.make(
	~listStyle="none",
	~overflowX="hidden",
	~overflowY="scroll",
	~maxWidth="90%",
	~maxHeight="78%",
	());

let speakerNameStyle = ReactDOMRe.Style.make(
	~flex="0 0 60px",
	~width="60px",
	~overflow="hidden",
	~margin="0",
	());

let messageStyle = ReactDOMRe.Style.make(
	~flex="1",
	~width="200px",
	~paddingLeft="20px",
	());

let entry = (user: string, message: string) => {
	<div style={entryStyle}>
		<div style={speakerNameStyle}>
			{ReasonReact.string(user)}
		</div>
		<div style={messageStyle}>
			{ReasonReact.string(message)}
		</div>
	</div>
};

type state = {
	isScrolledDown: bool,
};

type actions =
	| ScrolledBottom
	| ScrolledOff
	;

let onScroll = (send: actions => unit, uiEvent: ReactEvent.UI.t) => {
	let e = ReactEvent.UI.target(uiEvent)
	let scrollPosition = e##scrollTop;
	let fullHeight = e##scrollHeight;
	let visibleHeight = e##clientHeight;
	if(scrollPosition == (fullHeight - visibleHeight)) {
		send(ScrolledBottom);
	} else {
		send(ScrolledOff);
	}
};

let getLogEnd = Webapi.Dom.Document.getElementById("log-end")
let keyOf = (msg: Chat_types.msg) => msg.friend ++ "_" ++ Int32.to_string(msg.id);

let component = ReasonReact.reducerComponent("MessageHistory");
let make = (~user: string, ~messages: list(Chat_types.msg), _children) => {
  ...component,
  initialState: () => {
  		isScrolledDown: true,
	},
	didUpdate: (oldAndNew) => {
		if(oldAndNew.oldSelf.state.isScrolledDown) {
			let logEnd: option(ElementRe.t) = getLogEnd(Webapi.Dom.document);
			switch(logEnd) {
				| Some(e) => { ElementRe.scrollIntoView(e); }
				| None => ()
			}
		}
	},
  reducer: (action: actions, _: state) => switch(action) {
		| ScrolledBottom => ReasonReact.Update({isScrolledDown: true});
		| ScrolledOff => ReasonReact.Update({isScrolledDown: false});
	},
  render: self => {
    let msgs =
    	List.mapi(
      	(i: int, msg: Chat_types.msg) => {
					let evenOdd = Int32.rem(Int32.of_int(i), Int32.of_int(2))==Int32.of_int(0);
					<li style={liStyle(evenOdd)} key={keyOf(msg)}>{entry(msg.sender, msg.text)}</li>
				},
				messages,
      );
    <div id={"message-history"}>
    	<h1 style={headerStyle}> {ReasonReact.string(user)} </h1>
    	<ul style={ulStyle} onScroll={onScroll(self.send)}>
    		{ReasonReact.array(Array.of_list(msgs))}
    		<li id={"log-end"} style={ReactDOMRe.Style.make(~height="0", ())}></li>
			</ul>
		</div>;
  },
};


