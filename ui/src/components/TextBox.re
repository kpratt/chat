let textboxStyle = ReactDOMRe.Style.make(
	~border="3px solid #AAF",
	~borderRadius="4px",
	~width="100%",
	~height="100%",
	());

let unwrapUnsafely = data =>
  switch (data) {
  | Some(v) => v
  | None => raise(Invalid_argument("unwrapUnsafely called on None"))
  };

type state = string;

type actions =
  | Update(string)
  | Clear;

let component = ReasonReact.reducerComponent("TextBox");

let keyIsPrintable = (keycode: int) =>
  keycode > 47
  && keycode < 58  /* number keys */
  || keycode == 32
  || keycode == 13  /* spacebar & return key(s) (if you want to allow carriage returns) */
  || keycode > 64
  && keycode < 91  /* letter keys */
  || keycode > 95
  && keycode < 112  /* numpad keys */
  || keycode > 185
  && keycode < 193  /* ;=,-./` (in order) */
  || keycode > 218
  && keycode < 223;

let make = (~onEnter, _children) => {
  ...component,
  initialState: () => "",
  reducer: (action, _) =>
    switch (action) {
    | Update(value) => ReasonReact.Update(value)
    | Clear => ReasonReact.Update("")
    },
  render: self => {
    let f = (e: ReactEvent.Keyboard.t): unit => {
      let isShifted = ReactEvent.Keyboard.shiftKey(e);
      let key = ReactEvent.Keyboard.key(e);
      let keycode = ReactEvent.Keyboard.keyCode(e);
      let value = ReactEvent.Keyboard.target(e)##value;

      if (key == "Backspace") {
        self.send(Update(String.sub(value, 0, String.length(value) - 1)));
      } else if (isShifted && key == "Enter") {
        self.send(Update(value ++ "\n"));
      } else if (key == "Enter") {
        onEnter(value);
        self.send(Clear);
      } else if (keyIsPrintable(keycode)) {
        self.send(Update(value ++ key));
      };
    };

    <div onKeyDown=f> <textarea style={textboxStyle} value={self.state} /> </div>;
  },
};
