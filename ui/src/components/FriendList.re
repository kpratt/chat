let component = ReasonReact.statelessComponent("FriendList");

let listStyle = ReactDOMRe.Style.make(
	~listStyle="none",
	());

let entryStyle = ReactDOMRe.Style.make(
	~padding="8px",
	~maxWidth="90%",
	~overflow="hidden",
	());

let make = (~onFriendSelect, ~friends, _children) => {
  ...component,
  render: _ => {
    let onClick = (friend: string, _: ReactEvent.Mouse.t) => onFriendSelect(friend);
    let friendsElems =
      List.map(friend => <li style={entryStyle} key=friend onClick={onClick(friend)}> {ReasonReact.string(friend)} </li>, friends);

    <div>
      <h1> {ReasonReact.string("Friends (Online)")} </h1>
      <ul style={listStyle}> {ReasonReact.array(Array.of_list(friendsElems))} </ul>
    </div>;
  },
};
