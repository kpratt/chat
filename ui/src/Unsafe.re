let unwrap = (data) =>
    switch data {
    | Some(v) => v
    | None => raise(Invalid_argument("Unsafe.unwrap called on None"))
    };
