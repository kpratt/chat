
let default = (d: 'a, v: option('a)): 'a => {
  switch v {
      | Some(x) => x
      | None => d
      }
};

let noop = (_: 'a): unit => ();
let noop2 = (_: 'a, _: 'b): unit => ();
