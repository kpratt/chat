module StringMap =
  Map.Make({
    type t = string;
    let compare = compare;
  });

type postMethod = Chat_types.msg => unit;

let safeFind = (key: string, m: StringMap.t('a)): option('a) =>
  if (StringMap.exists((k, _) => k == key, m)) {
    Some(StringMap.find(key, m));
  } else {
    None;
  };

type privateMessageRoom = {
  friend: string,
  messages: list(Chat_types.msg),
};

type privateMessageRooms = StringMap.t(privateMessageRoom);

type state = {
  fp: string,
  currentWindow: option(string), /* active friend */
  friends: list(string),
  rooms: privateMessageRooms,
  post: option(postMethod),
};

type actions =
  | NoOp
  | WhoAmI(string)
  | CreateSocket
  | NewSocket(postMethod)
  | RoomChange(string) /* friend FP */
  | MessageRecieved(Chat_types.msg) /* (user, msg) */
  | MessageSent(string)
  | FriendArrived(string)
  | FriendLeft(string);
