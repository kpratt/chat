module Internal = {
  module TextEncoding = {
    type t;
    [@bs.new] external make: string => t = "TextEncoder";
    [@bs.send.pipe: t] external encode: string => Js.Typed_array.array_buffer = "encode";
  };

  module TextDecoding = {
    type t;
    [@bs.new] external make: string => t = "TextDecoder";
    [@bs.send.pipe: t] external decode: Js.Typed_array.array_buffer => string = "decode";
  };
};

module UTF8 = {
  let encoder = Internal.TextEncoding.make("utf8");
  let decoder = Internal.TextDecoding.make("utf8");

  let encode = (str: string): Js.Typed_array.array_buffer => Internal.TextEncoding.encode(str, encoder);
  let decode = (buf: Js.Typed_array.array_buffer): string => Internal.TextDecoding.decode(buf, decoder);
};
