open WebSockets;
/* now we need protobuf */
module ChatMessageEvent = {
  type t = Chat_types.frame;
};

let makeChatSocket = (send: Types.actions => unit): (Chat_types.msg => unit) => {
  Js.log("makeing socket");
  let ws = WebSocket.make("ws://" ++ Webapi.Dom.Location.host(Webapi.Dom.location) ++ "/socket");
  let handleOpen = () => ();
  let handleClose = _ => ();
  let handleMessage = (evt: MessageEvent.t) => {
    let json = MessageEvent.data(evt) |> Text.UTF8.decode;
    let frame = json |> Decoding.ofFrame;
    Js.log(frame);
    switch (frame) {
    | Id(id) =>
      Js.log("Identity FP " ++ id.fp ++ " recieved.");
      send(WhoAmI(id.fp));
    | Msg(msg) =>
      Js.log("Msg " ++ msg.text ++ " from [" ++ msg.friend ++ "]");
      send(Types.MessageRecieved(msg));
    | Friend_event(event) =>
      Js.log("Friend arrived " ++ event.friend);
      switch (event.event_type) {
      | Arrival => send(Types.FriendArrived(event.friend))
      | Departure => send(Types.FriendLeft(event.friend))
      };
    };
    ();
  };
  let handleError = errmsg => {
    Js.log(errmsg);
    ();
  };
  let post = msg => {
    ws |> WebSocket.sendString(msg |> Encoding.of_);
    ();
  };
  ws
  |> WebSocket.setBinaryType(ArrayBuffer)
  |> WebSocket.on(Open(handleOpen))
  |> WebSocket.on(Close(handleClose))
  |> WebSocket.on(Message(handleMessage))
  |> WebSocket.on(Error(handleError))
  |> ignore;

  post;
};
