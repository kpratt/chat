open Jest;

describe("Msg", () => {
  open Expect;

  test(" |> encode |> decode", () => {
    let msg = {
        Chat_types.id: Int32.of_int(0),
        Chat_types.friend: "5348ABF234",
        Chat_types.sender: "AB48ABF289",
        Chat_types.text: "Hello, world!",
    };
    expect(msg |> Encoding.of_ |> Decoding.of_) |> toEqual(msg)
  });

  test(" |> encode", () => {
    let msg = {
        Chat_types.id: Int32.of_int(0),
        Chat_types.friend: "5348ABF234",
        Chat_types.sender: "AB48ABF289",
        Chat_types.text: "Hello, world!",
    };
    expect(msg |> Encoding.of_ )
      |> toBe("{\"id\":0,\"friend\":\"5348ABF234\",\"sender\":\"AB48ABF289\",\"text\":\"Hello, world!\"}")
  });
});

describe("Frame", () => {
  open Expect;

	test(" |> encode |> decode", () => {
    let frame: Chat_types.frame = Chat_types.Friend_event({
    				Chat_types.friend: "5348ABF234",
    				Chat_types.event_type: Chat_types.Arrival,
    		});
    expect(frame |> Encoding.ofFrame |> Decoding.ofFrame) |> toEqual(frame)
  });

  test(" |> encode ", () => {
		let frame: Chat_types.frame = Chat_types.Friend_event({
				Chat_types.friend: "5348ABF234",
				Chat_types.event_type: Chat_types.Arrival,
		});
		expect(frame |> Encoding.ofFrame ) |> toEqual("{\"friendEvent\":{\"eventType\":\"Arrival\",\"friend\":\"5348ABF234\"}}")
	})

});

