open Jest;

describe("UTF8", () =>
  Expect.(
    test("str |> encode |> decode", () => {
      let str = "hello world";
      expect(str |> Text.UTF8.encode |> Text.UTF8.decode) |> toEqual(str);
    })
  )
);
