package main

import (
	"net/http"
	"log"
	"bitbucket.org/kpratt/chat/module"
	"fmt"
	"flag"
	"bitbucket.org/kpratt/nom-de-comm/pkg/link/client"
	"bitbucket.org/kpratt/nom-de-comm/pkg/types"
	"encoding/hex"
)

var port = flag.Uint("port", 8080, "The port to open the server on.")
var linkport = flag.Uint("link-port", 6006, "The port for f2f-linkd applications")

func main() {
	flag.Parse()

	f2f, err := f2fclient.NewWithListener(chat_module.AppUUID, int(*linkport), func(fps types.FP, b bool) {
		h := hex.EncodeToString(fps[:])
		fmt.Printf("Arrival: %s %t \n", h, b)
	})
	if err != nil {
		panic(err)
	}

	fmt.Printf("Listening on port %d.\n", *port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d",*port), chat_module.AccessLogger(chat_module.Router(f2f))))
}

