package chat_module

import (
	"bitbucket.org/atlassian/go-stack/router"
	"net/http"
)

func Router(f2f F2F) http.Handler {
	router := router.New()

	hub := newHub(f2f)
	go hub.local()
	go hub.remoteRead()
	// load UI
	router.Get("/", ServeIndex)
	router.Get("/assets/*path", ServeAssets)
	router.Mount("/socket", http.HandlerFunc(hub.ChatSocket))

	// JSON / REST ////
	///////////////////


	// load actual messages or listen for new messages
	return router
}
