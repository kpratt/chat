package chat_module

import (
	"log"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
	"encoding/json"
	"bitbucket.org/kpratt/chat/module/protocol/proto"
	"fmt"
	"encoding/hex"
	"bytes"
	"github.com/golang/protobuf/jsonpb"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 512
)

var (
	newline = []byte{'\n'}
	space   = []byte{' '}
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

// Client is a middleman between the websocket connection and the hub.
type Client struct {
	hub  *Hub
	conn *websocket.Conn
	send chan []byte
}

func (c *Client) readPump() {
	defer func() {
		c.hub.unregister <- c
		c.conn.Close()
	}()
	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error { c.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {
		_, bs, err := c.conn.ReadMessage()
		log.Printf("%s", string(bs))
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("error: %v", err)
			}
			break
		}

		chatMsg := &chat_protocol.Msg{}
		err = json.Unmarshal(bs, chatMsg)
		if err != nil {
			log.Printf("Msg: %s", string(bs))
			panic(err)
		}

		c.hub.browsers <- wrapMsg(chatMsg)
		c.hub.peer <- chatMsg
	}
}

func (c *Client) Send(frame *chat_protocol.Frame) {
	ma := jsonpb.Marshaler{}
	buf := &bytes.Buffer{}
	err := ma.Marshal(buf, frame)
	if err != nil {
		panic(err)
	}
	bs := buf.Bytes()
	c.send <- bs
}

// writePump pumps messages from the hub to the websocket connection.
//
// A goroutine running writePump is started for each connection. The
// application ensures that there is at most one writer to a connection by
// executing all writes from this goroutine.
func (c *Client) writePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.conn.Close()
	}()
	for {
		select {
		case message, ok := <-c.send:
			hx := hex.EncodeToString(message)
			fmt.Printf("Sending thing [ %s ] to client.\n", hx)
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// The hub closed the channel.
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			w, err := c.conn.NextWriter(websocket.BinaryMessage)
			if err != nil {
				return
			}
			w.Write(message)

			if err := w.Close(); err != nil {
				return
			}
		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

// serveWs handles websocket requests from the peer.
func (hub *Hub) ChatSocket(w http.ResponseWriter, r *http.Request) {
	log.Printf("WebSocket connected.")
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	client := &Client{hub: hub, conn: conn, send: make(chan []byte, 256)}
	client.hub.register <- client

	// Allow collection of memory referenced by the caller by doing all work in
	// new goroutines.
	go client.writePump()
	go client.readPump()
}
