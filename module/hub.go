//go:generate mockgen -destination=../mocks/mock-f2f.go -package=mocks bitbucket.org/kpratt/chat/module F2F

package chat_module

import (
	"github.com/golang/protobuf/proto"
	"bitbucket.org/kpratt/chat/module/protocol/proto"
	"encoding/hex"
	"bitbucket.org/kpratt/nom-de-comm/pkg/types"
	"strings"
	"github.com/golang/protobuf/jsonpb"
	"bytes"
	"github.com/google/uuid"
	"log"
)

type F2F interface {
	WhoAmI() types.FP
	Listen(f func(fp types.FP, online bool))
	Send(dst types.FP, dstApp uuid.UUID, payload []byte) error
	Recv() (*types.Frame, error)
	FriendsList() []types.FP
	Close() error
}

type Hub struct {
	// communication with remote friends
	f2f F2F

	// Registered clients.
	clients map[*Client]bool

	// Inbound messages from the clients.
	browsers chan *chat_protocol.Frame
	peer     chan *chat_protocol.Msg

	// Register requests from the clients.
	register chan *Client

	// Unregister requests from clients.
	unregister chan *Client
}

func newHub(f2f F2F) *Hub {
	return &Hub{
		f2f:        f2f,
		browsers:   make(chan *chat_protocol.Frame),
		peer:       make(chan *chat_protocol.Msg),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		clients:    make(map[*Client]bool),
	}
}

func wrapMsg(chatMsg *chat_protocol.Msg) *chat_protocol.Frame {
	return &chat_protocol.Frame{
		Inner: &chat_protocol.Frame_Msg{chatMsg},
	}
}

func identityFrame(fp types.FP) *chat_protocol.Frame {
	return &chat_protocol.Frame{
		Inner: &chat_protocol.Frame_Id{Id: &chat_protocol.Identity{
			Fp: hex.EncodeToString(fp[:]),
		}},
	}
}

func friendFrame(fp types.FP, etype chat_protocol.FriendEvent_EType) *chat_protocol.Frame {
	return &chat_protocol.Frame{
		Inner: &chat_protocol.Frame_FriendEvent{FriendEvent: &chat_protocol.FriendEvent{
			Friend:    hex.EncodeToString(fp[:]),
			EventType: etype,
		}},
	}
}

func (h *Hub) remoteRead() {
	h.f2f.Listen(func(fp types.FP, online bool) {
		etype := chat_protocol.FriendEvent_Departure
		if online {
			etype = chat_protocol.FriendEvent_Arrival
		}

		h.browsers <- friendFrame(fp, etype)
	})

	for {
		f2fFrame, err := h.f2f.Recv()
		if err != nil {
			panic(err)
		}

		chatMsg := &chat_protocol.Msg{}
		err = proto.Unmarshal(f2fFrame.Payload, chatMsg)
		if err != nil {
			panic(err)
		}

		chatFrame := &chat_protocol.Frame{
			Inner: &chat_protocol.Frame_Msg{chatMsg},
		}

		log.Printf("Fm:%s [ %s ]", chatMsg.Friend, chatMsg.Text)
		h.browsers <- chatFrame
	}
}

var AppUUID, _ = uuid.Parse("ac43eaf6-37fe-4bfb-94b5-a731ae959a0d")

func (h *Hub) local() {
	ma := jsonpb.Marshaler{}
	for {
		select {
		case client := <-h.register:
			h.clients[client] = true
			fp := h.f2f.WhoAmI()
			log.Printf("Who am i? %s", hex.EncodeToString(fp[:]))
			client.Send(identityFrame(fp))
			for _, friend := range h.f2f.FriendsList() {
				frame := friendFrame(friend, chat_protocol.FriendEvent_Arrival)
				log.Printf("Friend encoding [ %s ]", hex.EncodeToString(friend[:]))
				client.Send(frame)
			}
		case client := <-h.unregister:
			if _, ok := h.clients[client]; ok {
				delete(h.clients, client)
				close(client.send)
			}
		case message := <-h.browsers:
			buf := &bytes.Buffer{}
			err := ma.Marshal(buf, message)
			if err != nil {
				panic(err)
			}
			bs := buf.Bytes()
			for client := range h.clients {
				select {
				case client.send <- bs:
				default:
					close(client.send)
					delete(h.clients, client)
				}
			}
		case chatMsg := <-h.peer:
			log.Printf("To:%s [ %s ]", chatMsg.Friend, chatMsg.Text)
			if strings.Compare(chatMsg.Friend, "self") == 0 {
				continue
			}
			fp := types.FP{}
			_, err := hex.Decode(fp[:], []byte(chatMsg.Friend))
			if err != nil {
				panic(err)
			}
			pb, err := proto.Marshal(chatMsg)
			h.f2f.Send(fp, AppUUID, pb)
		}
	}
}
