package chat_module

import (
	"net/http"
	"log"
	"os"
	"bitbucket.org/kpratt/chat/module/assets"
	"strings"
)

// return the UI
func ServeIndex(w http.ResponseWriter, r *http.Request) {
	// dev mode serves fs
	_, err := os.Stat("assets/index.html")
	if err == nil {
		http.ServeFile(w, r, "assets/index.html")
	}

	// prod mode serves from bin
	bs, err := assets.Asset("index.html")
	if err != nil {
		w.WriteHeader(500)
		return
	}

	w.Write(bs)
}

func ServeAssets(w http.ResponseWriter, r *http.Request) {
	// dev mode serves fs
	_, err := os.Stat("./assets/index.html")
	if err == nil {
		fs := http.FileServer(http.Dir("assets"))
		handle := http.StripPrefix("/assets/", fs)
		handle.ServeHTTP(w, r)
		return
	}

	// prod mode serves from bin
	p := strings.TrimPrefix(r.URL.Path, "/assets/")
	if len(p) >= len(r.URL.Path) {
		http.NotFound(w, r)
		return
	}

	bs, err := assets.Asset(p)
	if err != nil {
		http.NotFound(w, r)
		return
	}

	w.Write(bs)
}

func PageNotFound(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNotFound)
	w.Write([]byte("<html><head></head><body><h1>404 error</h1></body></html>"))
}

///////// Helpers /////////

func AccessLogger(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("host %s accessed %s %s", r.RemoteAddr, r.Method, r.URL.Path)
		handler.ServeHTTP(w, r)
	})
}
